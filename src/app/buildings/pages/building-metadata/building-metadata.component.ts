import { Component, OnInit, Input } from '@angular/core';
import { AsyncValidatorFn, AbstractControl } from '@angular/forms';
import { tap } from 'rxjs/operators';

import { KeywordEditorConfig } from 'src/app/shared/keyword-editor/keyword-editor.types';

import { Building } from '../../service/building.types';
import { BuildingsService } from '../../service/buildings.service';

@Component({
  selector: 'app-building-metadata',
  templateUrl: './building-metadata.component.html',
  styleUrls: []
})
export class BuildingMetadataComponent implements OnInit {
  @Input()
  public building: Building;

  public nicknameEditorConfig: KeywordEditorConfig = {
    newButtonText: 'Add new nickname',
    inputPlaceholder: 'Enter a nickname',
    editorTitle: 'Nicknames',
    emptyStateText: "This building doesn't have any nicknames yet. Time to get creative!"
  }

  constructor(private buildingsService: BuildingsService) {}

  ngOnInit() {
    this.loadBuilding();
  }

  public validateNickname: AsyncValidatorFn = (control: AbstractControl) => {
    return this.buildingsService.isValidNickname(control.value);
  };

  public saveNicknames = (nicknameList: string[]) => {
    return this.buildingsService.update({
      ...this.building,
      nicknames: nicknameList
    }).pipe(
      tap(() => this.loadBuilding())
    );
  }

  private loadBuilding(): void {
    this.buildingsService.getById('1').subscribe(building => {
      this.building = building;
    });
  }
}
