import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildingMetadataComponent } from './building-metadata.component';
import { BuildingDetailsComponent } from '../../components/building-details/building-details.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';

describe('BuildingMetadataComponent', () => {
  let component: BuildingMetadataComponent;
  let fixture: ComponentFixture<BuildingMetadataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildingMetadataComponent, BuildingDetailsComponent ],
      imports: [CommonModule, SharedModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildingMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
