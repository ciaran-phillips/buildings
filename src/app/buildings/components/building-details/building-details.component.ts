import { Component, OnInit, Input } from '@angular/core';
import { Building } from '../../service/building.types';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';

import { BuildingsService } from '../../service/buildings.service';

@Component({
  selector: 'app-building-details',
  templateUrl: './building-details.component.html',
  styleUrls: ['./building-details.component.scss']
})
export class BuildingDetailsComponent {
  @Input()
  public building: Building;

  constructor(private BuildingsService: BuildingsService) {}
}
