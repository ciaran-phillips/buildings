import { Injectable } from '@angular/core';

import { of, Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

import { Building } from './building.types';
import { ValidationErrors } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class BuildingsService {

  private building: Building = {
    id: 1,
    address: "30 St Mary Axe, London",
    description: `Lorem ipsum dolor sit amet
and so on

with linebreaks
    `,
    nicknames: ['Building A', 'Head Quarters', 'City View']
  };

  public getById(id: string): Observable<Building> {
    return of(this.building);
  }

  public update(building: Building): Observable<Building> {
    this.building = building;
    return of(this.building).pipe(
      delay(500)
    );
  }

  public isValidNickname(nickname: string): Observable<ValidationErrors | null> {
    const valid = this.isValidNicknameMock(nickname);
  
    const res = valid ? of(null) : of({
      'custom': [{
        'type': 'conflict',
        'message': 'This nickname is already in use by another building:',
        'conflictingEntityLink': '/link/to/or/id/of/conflicting/building',
        'conflictingEntityName': '12 Smith Rd., Greenwich'
      }]
    });

    return res.pipe(
      delay(500)
    )
  }

  private isValidNicknameMock(nickname: string): boolean {
    const normalised = nickname.trim();
    if (nickname[0] === 'a') {
      return false;
    }
    return true;
  }
}
