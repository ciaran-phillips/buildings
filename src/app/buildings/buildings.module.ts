import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';

import { BuildingMetadataComponent } from './pages/building-metadata/building-metadata.component';
import { BuildingDetailsComponent } from './components/building-details/building-details.component';

@NgModule({
  declarations: [BuildingMetadataComponent, BuildingDetailsComponent],
  imports: [
    CommonModule, 
    SharedModule
  ],
  exports: [
    BuildingMetadataComponent
  ]
})
export class BuildingsModule { }
