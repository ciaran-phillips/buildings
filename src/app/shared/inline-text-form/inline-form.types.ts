import { FormGroup } from '@angular/forms';

export interface InlineForm {
    inEditMode: boolean;
    form: FormGroup;
    saving: boolean;
    deleting: boolean;
    placeholder: string;
}