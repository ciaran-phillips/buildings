import { Component, Input, Output, EventEmitter } from '@angular/core';

import { InlineForm } from './inline-form.types';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-inline-text-form',
  templateUrl: './inline-text-form.component.html',
  styleUrls: ['./inline-text-form.component.scss']
})
export class InlineTextFormComponent {
  @Input()
  public formConfig: InlineForm;

  @Output()
  public formSubmit = new EventEmitter<string>();

  @Output()
  public formCancel = new EventEmitter<void>();

  public onSubmit(): void {
    this.formSubmit.emit(this.formConfig.form.value.textField);
  }

  public onCancel(): void {
    this.formCancel.emit();
  }

  public getErrors(formConfig: InlineForm): ValidationErrors {
    return formConfig.form.controls.textField.errors;
  }
}
