import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { InlineTextFormComponent } from './inline-text-form.component';
import { OnKeyupDirective } from '../on-keyup/on-keyup.directive';
import { AutofocusDirective } from '../autofocus/autofocus.directive';
import { FormsModule, ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';
import { asyncData } from 'src/app/testing/helpers';

describe('InlineTextFormComponent', () => {
  let component: InlineTextFormComponent;
  let fixture: ComponentFixture<InlineTextFormComponent>;
  let defaultFormConfig;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InlineTextFormComponent, OnKeyupDirective, AutofocusDirective ],
      imports: [ FormsModule, ReactiveFormsModule ]
    })
    .compileComponents();

    defaultFormConfig = {
      name: '',
      inEditMode: true,
      form: new FormGroup({ textField: new FormControl('') }),
      saving: false,
      deleting: false,
      placeholder: 'my placeholder text'
    };
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineTextFormComponent);
  });

  it('should create', () => {
    component = fixture.componentInstance;
    component.formConfig = defaultFormConfig;
    fixture.detectChanges();


    expect(component).toBeTruthy();
  });

  it('should should set the input value', () => {
    const textContent = 'sample value';
    component = fixture.componentInstance;
    component.formConfig = { 
      ...defaultFormConfig,
      form: new FormGroup({ textField: new FormControl(textContent) })
    }

    fixture.detectChanges();
    const elem = fixture.nativeElement;
    const input = elem.querySelector('input');
    expect(input.value).toEqual(textContent);
  });


  it('should emit the value on submit button clicked', () => {
    const inputValue = 'sample value';
    let outputValue = '';
    component = fixture.componentInstance;
    component.formConfig = {
      ...defaultFormConfig,
      form: new FormGroup({ textField: new FormControl(inputValue)} )
    };

    fixture.detectChanges();
    const elem = fixture.nativeElement;
    component.formSubmit.subscribe(newValue => { outputValue = newValue; });
    elem.querySelector('button[type="submit"]').click();
    expect(inputValue).toEqual(outputValue);
  });

  it('should call cancel callback when cancel button clicked', () => {
    const inputValue = 'sample value';
    let cancelCalled = false;
    component = fixture.componentInstance;
    component.formConfig = defaultFormConfig;

    fixture.detectChanges();
    const elem = fixture.nativeElement;
    component.formCancel.subscribe(() => { cancelCalled = true });
    elem.querySelector('button[type="button"]').click();
    expect(cancelCalled).toBeTruthy();
  })


  it('should display spinner when saving', () => {
    const inputValue = 'sample value';
    let spinner;
    component = fixture.componentInstance;
    component.formConfig = defaultFormConfig;

    fixture.detectChanges();
    const elem = fixture.nativeElement;
    spinner = elem.querySelector('button[type="submit"] .fa-spinner');
    expect(spinner).toBeNull();

    component.formConfig = {
      ...component.formConfig,
      saving: true
    };
    fixture.detectChanges();
    spinner = elem.querySelector('button[type="submit"] .fa-spinner');
    expect(spinner).toBeTruthy();
  })

  it('should display validation indicator when validating', fakeAsync(() => {
    const inputValue = 'sample value';
    // returning null indicates successful validation (no error returned)
    let validatorFn = () => asyncData(null);
    component = fixture.componentInstance;
    component.formConfig = {
      ...defaultFormConfig,
      form: new FormGroup({ textField: new FormControl(inputValue, [], validatorFn) })
    };

    fixture.detectChanges();
    const elem = fixture.nativeElement;
    const indicator = elem.querySelector('.validation-notice');
    expect(indicator.textContent).toContain('Validating');
    tick();
    fixture.detectChanges();
    expect(elem.querySelector('.validation-notice')).toBeNull();
  }));


  it('should disable submit while validating', fakeAsync(() => {
    const inputValue = 'sample value';
    // returning null indicates successful validation (no error returned)
    let validatorFn = () => asyncData(null);
    component = fixture.componentInstance;
    component.formConfig = {
      ...defaultFormConfig,
      form: new FormGroup({ textField: new FormControl(inputValue, [], validatorFn) }),
    };

    fixture.detectChanges();
    const elem = fixture.nativeElement;

    // initial setup - validation should start immediately
    const submitBtn = elem.querySelector('button[type="submit"]');
    expect(submitBtn.disabled).toBeTruthy();

    // finish validation
    tick();
    fixture.detectChanges();
    expect(submitBtn.disabled).toBeFalsy();

    // change value so that validation starts again
    component.formConfig.form.controls.textField.setValue('testing');
    fixture.detectChanges();
    expect(submitBtn.disabled).toBeTruthy();

    // finish validation
    tick();
    fixture.detectChanges();
    expect(submitBtn.disabled).toBeFalsy();
  }));


  it('should show message when validation fails', fakeAsync(() => {
    const inputValue = 'sample value';
    // returning null indicates successful validation (no error returned)
    const testError = {
      type: 'conflict',
      message: 'my message',
      conflictingEntityLink: '',
      conflictingEntityName: 'entity name'
    };
    let validatorFn = () => asyncData({ custom: [testError] });
    component = fixture.componentInstance;
    component.formConfig = {
      ...defaultFormConfig,
      form: new FormGroup({ textField: new FormControl(inputValue, [], validatorFn) })
    };

    fixture.detectChanges();
    const elem = fixture.nativeElement;

    const validationErrorClass = '.validation-error';
    // initial setup - validation should start immediately
    expect(elem.querySelector(validationErrorClass)).toBeFalsy();

    // finish validation
    tick();
    fixture.detectChanges();
    const err = elem.querySelector(validationErrorClass);
    expect(err.textContent).toContain(testError.message);
    expect(err.querySelector('a').textContent).toContain(testError.conflictingEntityName);
  }));
});
