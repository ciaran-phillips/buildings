import { Component, Input, OnChanges } from '@angular/core';
import { FormGroup, FormControl, AsyncValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

import { InlineForm } from '../inline-text-form/inline-form.types';

import { KeywordEditorConfig, Deletion } from './keyword-editor.types';

@Component({
  selector: 'app-keyword-editor',
  templateUrl: './keyword-editor.component.html',
  styleUrls: ['./keyword-editor.component.scss']
})
export class KeywordEditorComponent implements OnChanges {
  @Input()
  public config: KeywordEditorConfig;

  @Input()
  public keywords: string[];

  @Input()
  public validationCallback: AsyncValidatorFn;

  @Input()
  public saveCallback: (keywords: string[]) => Observable<boolean>;

  public latestDeletions: Deletion[] = [];

  public newKeywordFormConfig: InlineForm;

  public keywordForms: InlineForm[];

  public ngOnChanges(): void {
    const defaults = {
      inEditMode: false,
      saving: false,
      deleting: false,
      placeholder: this.config.inputPlaceholder
    };

    this.newKeywordFormConfig = {
      form: new FormGroup({
        textField: new FormControl('', [], this.validationCallback)
      }),
      ...defaults
    };

    this.keywordForms = this.keywords.map(keyword => ({
      form: new FormGroup({
        textField: new FormControl(keyword, [], this.validationCallback)
      }),
      ...defaults
    }));
  }

  public addNewKeyword(value: string): void {
    const newList = [value, ...this.keywords];
    this.saveCallback(newList).subscribe(() => {
      this.newKeywordFormConfig.inEditMode = false;
    });
  }

  public updateKeyword(index: number, value: string): void {
    const keywordFormConfig = this.keywordForms[index];
    keywordFormConfig.saving = true;

    const updatedList = [
      ...this.keywords.slice(0, index),
      value,
      ...this.keywords.slice(index + 1)
    ];

    this.saveCallback(updatedList)
      .subscribe(() => {
        keywordFormConfig.saving = false;
      });
  }

  public delete(index): void {
    const keywordFormConfig = this.keywordForms[index];
    keywordFormConfig.deleting = true;
    const updatedList = this.keywords.filter((_, i) => i !== index);
    const deletionTimestamp = Date.now();
    this.saveCallback(updatedList)
      .pipe(
        tap(() => {
          keywordFormConfig.deleting = false;
          this.latestDeletions.push({ 
            index,
            id: deletionTimestamp, 
            keyword: keywordFormConfig.form.value.textField,
            undoInProgress: false
          });
        }),
        delay(8000),
        tap(() => { 
          this.latestDeletions = this.latestDeletions.filter(
            deletion => deletion.id !== deletionTimestamp);
        })
      )
      .subscribe();
  }

  public undoDeletion(deletion: Deletion): void {
    const newKeywordList = [
      ...this.keywords.slice(0, deletion.index),
      deletion.keyword,
      ...this.keywords.slice(deletion.index)
    ];
    deletion.undoInProgress = true;

    this.saveCallback(newKeywordList)
      .subscribe(() => {
        this.latestDeletions = this.latestDeletions.filter(currentDel => currentDel.id !== deletion.id);
      });
  }

  public toggle(index: number): void {
    this.keywordForms[index].inEditMode = !this.keywordForms[index].inEditMode;
    // reset the value, so that we don't preserve the entered text when canceling the edit
    this.keywordForms[index].form.controls.textField.setValue(this.keywords[index]);
  }

  public toggleNewKeywordForm(): void {
    this.newKeywordFormConfig.inEditMode = !this.newKeywordFormConfig.inEditMode;
    this.newKeywordFormConfig.form.controls.textField.setValue('');
  }
}
