export interface KeywordEditorConfig {
  newButtonText: string;
  inputPlaceholder: string; 
  editorTitle: string;
  emptyStateText: string;
}

export interface Deletion {
  id: number;
  keyword: string;
  index: number;
  undoInProgress: boolean;
}