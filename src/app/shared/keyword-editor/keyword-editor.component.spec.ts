import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { KeywordEditorComponent } from './keyword-editor.component';
import { InlineTextFormComponent } from '../inline-text-form/inline-text-form.component';
import { OnKeyupDirective } from '../on-keyup/on-keyup.directive';
import { AutofocusDirective } from '../autofocus/autofocus.directive';

describe('KeywordEditorComponent', () => {
  let component: KeywordEditorComponent;
  let fixture: ComponentFixture<KeywordEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeywordEditorComponent, InlineTextFormComponent, OnKeyupDirective, AutofocusDirective ],
      imports: [ FormsModule, ReactiveFormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeywordEditorComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
