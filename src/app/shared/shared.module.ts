import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

import { KeywordEditorComponent } from './keyword-editor/keyword-editor.component';
import { AutofocusDirective } from './autofocus/autofocus.directive';
import { InlineTextFormComponent } from './inline-text-form/inline-text-form.component';
import { OnKeyupDirective } from './on-keyup/on-keyup.directive';

@NgModule({
  declarations: [KeywordEditorComponent, AutofocusDirective, InlineTextFormComponent, OnKeyupDirective],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTooltipModule
  ],
  exports: [
    KeywordEditorComponent,
    AutofocusDirective,
    OnKeyupDirective
  ]
})
export class SharedModule { }
