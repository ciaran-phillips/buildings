import { Directive, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appOnKeyup]'
})
export class OnKeyupDirective {
  @Input()
  public appOnKeyup: number;

  @Output()
  public keyupDetected = new EventEmitter<void>();

  constructor(private element: ElementRef) {}

  @HostListener('window:keyup', ['$event'])
  public reactToKeyUp($event: KeyboardEvent): void {
    if ($event.keyCode === this.appOnKeyup && $event.target === this.element.nativeElement) {
      this.keyupDetected.emit();
    }
  }
}
