import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BuildingsModule } from './buildings/buildings.module';
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    BuildingsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
