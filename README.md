# Building nickname editor

## Installation and run instructions

* clone the repo
* run `npm install`
* run `ng serve
* The app should be visible at `localhost:4200`

## Assumptions, notes

* I've used a single string field for the address. In practice this would normally be multiple fields.
* The sample description I'm using is quite short. If large descriptions were common, then we should either have 'see more' behaviour, or a slightly different design
* The layout currently gives more weight to nicknames than to address or description, which makes sense given the nicknames are editable here, and the other fields mostly provide context. However, it seems likely that this page would turn into a general overview, with editing of various bits of building metadata. In that case it would make sense for these section to have equal importance in the hierarchy

## Things to improve on

* Test coverage is very low - only covering the inline form at the moment (and not all possible scenarios for that). A lot of tests should still be written for the keyword editor component
* There is no reordering of the nicknames. It's unclear whether the order should be important, as I don't know the context they'd be used in. If nicknames are intended to be used as a 'display name' in place of the address, then it would 
* Many other small issues that were still on the to do list when I ran out of time for working on this. For example:
    * No validation against empty nicknames
    * Too much code in the keyword editor component (functions for editing the keyword array could easily be factored out)
    * Some missing margins when on a mobile view
    * Jumpy animations, as I didn't add special states for items just added to or being removed from the DOM.